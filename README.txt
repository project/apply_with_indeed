CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Difference from Others
 * Faqs

INTRODUCTION
------------
This extends job applies with additional option "Apply with Indeed" account.
Feature allows user to apply your site job with their Indeed account details 
and you uses Indeed "PostUrl" method to send applicant details at backend to 
perform further operations.

FEATURES
--------
* Multilingual support.
* After successful job apply, sends Applicant details at backend for further processes.
* Also, can redirect user to Thank you page.
* Phone number, cover letter options configurable on apply form.

REQUIREMENTS
------------
 * To use this feature, you should have Indeed API Token to start indeed widget on your application.
 * Use https://secure.indeed.com/account/login to get access token.

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------
 Go to Administration � Structure � Configuration � Apply with Indeed settings
    * Configure Indeed API token key, your site company name.
    * Configure site's job application form id, where need to display Indeed button.
    * Make other optional configuration for phone number and cover letter.
    * Do save configuration
    * Enjoy Apply with Indeed button on job application page.

DIFFERENCE FROM OTHERS
----------------------
    * It's new feature, presently no alternative module available in market.

FAQ'S
----
1- Would is also compatible for Mobile devices?
    * Yes, it's support all type of desktop and mobile devices.

2- Is it allow an applicant to apply on a job multiple times?
    * Yes, that feature is configurable, if your site requirement is a user can
      apply a job multiple times, then you can enable it. Default is FALSE.

3- What are the applicant details sending at backend?
    * When an applicant applied to a job, It's send applicant personal details
      like Fullname, Email, Phone number, Resume file to backend. That helps to
      you to make futher process at backend.
