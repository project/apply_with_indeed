<?php

/**
 * @file
 * Hooks provided by the apply_with_indeed module.
 */

/**
 * Implements hook_indeed_update_job_details().
 *
 * Allow other modules to change job details to show on Indeed job apply form.
 *
 * @param object $job_details
 *   Pass Job details that user is applying for.
 *    'nid' - Job node id (required)
 *    'title' - Job title (required)
 *    'location' - Job posting location (optional)
 *    'url' - Job detail page URL (optional)
 *    'thankyou_page_url' - Site thank you page URL (optional)
 */
function hook_indeed_update_job_details(&$job_details) {
  // The apply with indeed module pass applying job information to Indeed widget
  // To show on Indeed job apply form.
}

/**
 * Implements hook_indeed_application_submission().
 *
 * Use this hook to process applicant details to submit application on your website.
 *
 * @param array $applicant_details
 *   This consist applicant details. Eg
 *    'Full name', 'Email', 'Phone number', 'CV file'.
 */
function hook_indeed_application_submission($applicant_details) {
  // This hook allow you to process the applicant details and get use them
  // as per your business requirement.
}
