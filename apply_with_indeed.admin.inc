<?php

/**
 * @file
 * Apply job with Indeed related admin configuration.
 */

/**
 * Implements hook_form().
 *
 * Indeed job apply feature configuration.
 */
function apply_with_indeed_admin_settings_form($form, $form_state) {
  $form['indeed_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Indeed API Token.'),
    '#required' => TRUE,
    '#default_value' => variable_get('indeed_api_token'),
    '#description' => t('Enter Indeed api token.'),
  );

  $form['indeed_site_company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Company Name.'),
    '#required' => TRUE,
    '#default_value' => variable_get('indeed_site_company_name'),
    '#description' => t('Company name will display on Indeed apply form with job title.'),
  );

  $form['indeed_job_apply_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Job apply form Id, where Apply with Indeed display.'),
    '#required' => TRUE,
    '#default_value' => variable_get('indeed_job_apply_form_id'),
    '#description' => t('Enter site\'s job application form ID, where this'
      . ' button will display for users. eg. "my_site_job_apply_form"'),
  );

  $form['indeed_display_phone_num_on_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Phone number field on Indeed apply form.'),
    '#default_value' => variable_get('indeed_display_phone_num_on_form'),
    '#description' => t('This wll display Phone number field on indeed job apply form.'),
  );

  $form['indeed_mandatory_phone_num_on_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make Phone number field mandatory on Indeed apply form.'),
    '#default_value' => variable_get('indeed_mandatory_phone_num_on_form'),
    '#description' => t('Make phone number field required.'),
  );

  $form['indeed_display_cover_letter_on_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display cover letter field on Indeed apply form.'),
    '#default_value' => variable_get('indeed_display_cover_letter_on_form'),
    '#description' => t('This wll display cover letter field on indeed job apply form.'),
  );

  $form['indeed_mandatory_cover_letter_on_form'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make cover letter field mandatory on Indeed apply form.'),
    '#default_value' => variable_get('indeed_mandatory_cover_letter_on_form'),
    '#description' => t('Make cover letter field required.'),
  );

  $form['indeed_allow_multiple_apply_on_job'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow a user to apply multiple times on a job.'),
    '#default_value' => variable_get('indeed_allow_multiple_apply_on_job'),
    '#description' => t('If checked, a user can apply multiple times on a job.'),
  );

  return system_settings_form($form);
}
